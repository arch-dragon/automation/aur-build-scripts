#!/bin/bash

package=$1
arch=$(uname -m)

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
CONFIG_DIR="$SCRIPT_DIR/../config"

# setup pacman 
"$SCRIPT_DIR/setuppacman.sh" $arch

# prepare user and paru
groupadd -g 985 users
groupadd -g 998 wheel
useradd -m builder -G wheel
pacman -S --noconfirm --needed base-devel git unzip wget

echo "%wheel ALL=(ALL) NOPASSWD: ALL" | tee -a /etc/sudoers

#try to install built paru from gitlab jobs

mkdir temp
cd temp

paruDownload="paru-static"
paruBuild="paru-bin"

pacman -Sy --config "$CONFIG_DIR/pacman-deps-${arch}.conf" --noconfirm $paruDownload
if [ $? -eq 0 ]; then
    installed="y"
    echo "Installed prebuilt $paruDownload"
else
    echo "Cannot retrieve prebuilt $paruDownload"
fi

cd ../
rm -rf temp

#install paru if built version is not available

if [ -z "$installed" ]; then
    echo "$paruBuild will be built"
    git clone https://aur.archlinux.org/$paruBuild.git
    cd $paruBuild
    chmod 777 -R ./
    su builder -c "makepkg -si --noconfirm"
    cd ..
    rm -rf $paruBuild
fi

# install additional dependencies
pacman -S --noconfirm --needed tree gawk
#pacman -R --noconfirm ffmpeg

# import keys
if [ "$package" == "tor-browser" ]; then
    su builder -c "gpg --auto-key-locate nodefault,wkd --locate-keys torbrowser@torproject.org"
fi