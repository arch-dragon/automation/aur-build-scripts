#!/bin/bash

packageName=$1
arch=$2

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
CONFIG_DIR="$SCRIPT_DIR/../config"
REPOLIST=repolist-$arch.txt

if [ ! -f "$REPOLIST" ]; then
    paru -Sl archdragon-aur --config "$CONFIG_DIR/pacman-deps-${arch}.conf" > "$REPOLIST"
fi

paru -G $packageName

# probably failed to fetch package from aur
if [ ! $? -eq 0 ]; then
    exit 2
fi

folder=$(ls -1 --sort=time | head -n 1)
user=$(cat /etc/passwd | grep -i 1000 | cut -d: -f1)

chmod 777 -R $folder
cd $folder

if $(echo "$packageName" | grep -iq "\-git") ; then
    echo "Downloading sources to help determine package version"
    su builder -c "makepkg -s -r --nobuild --noconfirm"
fi

su builder -c "makepkg --printsrcinfo > .SRCINFO"

pkgname=$packageName
pkgver=$(cat .SRCINFO | grep "pkgver = " | sed "s/'//g" )
pkgrel=$(cat .SRCINFO | grep "pkgrel = " | sed "s/'//g" )

if cat .SRCINFO | grep -qi 'epoch ='; then
    epoch=$(cat .SRCINFO | grep "epoch = " | sed "s/'//g" )
    fullname="$pkgname ${epoch:9}:${pkgver:10}-${pkgrel:10}"
else
    fullname="$pkgname ${pkgver:10}-${pkgrel:10}"
fi


cd ..

rm -rf $folder

echo "$fullname"

if grep -q "$fullname" "$REPOLIST" ; then
    exit 0 # is up to date
else
    exit 1 # is not up to date
fi