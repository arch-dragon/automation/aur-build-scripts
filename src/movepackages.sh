#!/bin/bash

echo "Finding built packages"
files=$(tree -fia /home/builder/.cache/paru | grep -i "\.pkg\.")

echo "${files}" | tee -a files.txt
echo "" | tee -a files.txt

mkdir packages

echo "Started gathering packages"
while read package; do 
    echo "moved ${package} to packages"
    mv "${package}" packages/ 
done < files.txt

rm files.txt

ls -l packages