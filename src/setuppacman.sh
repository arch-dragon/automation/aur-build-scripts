#!/bin/bash

arch=$1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [ "$arch" == "x86_64" ]; then
    echo "Setting up mirrors"
    rm /etc/pacman.d/mirrorlist

    echo '
    #Server = https://mirrors.dotsrc.org/archlinux/$repoName/os/$arch
    Server = https://ftp.halifax.rwth-aachen.de/archlinux/$repo/os/$arch
    Server = https://mirror.ubrco.de/archlinux/$repo/os/$arch
    Server = https://mirror.chaoticum.net/arch/$repo/os/$arch
    Server = https://mirror.fra10.de.leaseweb.net/archlinux/$repo/os/$arch
    Server = https://mirror.ams1.nl.leaseweb.net/archlinux/$repo/os/$arch
    Server = https://packages.oth-regensburg.de/archlinux/$repo/os/$arch
    Server = https://arch.mirrors.lavatech.top/$repo/os/$arch
    Server = https://mirror.f4st.host/archlinux/$repo/os/$arch
    Server = https://mirror.bethselamin.de/$repo/os/$arch
    Server = https://archmirror.it/repos/$repo/os/$arch
    Server = https://ftp.fau.de/archlinux/$repo/os/$arch
    Server = https://mirror.cyberbits.eu/archlinux/$repo/os/$arch
    Server = https://archlinux.uk.mirror.allworldit.com/archlinux/$repo/os/$arch
    Server = https://gluttony.sin.cvut.cz/arch/$repo/os/$arch
    Server = https://archlinux.mailtunnel.eu/$repo/os/$arch
    Server = https://ftp.sh.cvut.cz/arch/$repo/os/$arch
    Server = https://mirrors.niyawe.de/archlinux/$repo/os/$arch
    Server = https://mirror.wtnet.de/arch/$repo/os/$arch
    Server = https://dist-mirror.fem.tu-ilmenau.de/archlinux/$repo/os/$arch
    Server = https://arch.jensgutermuth.de/$repo/os/$arch
    Server = https://arch.mirror.zachlge.org/$repo/os/$arch
    Server = https://mirror.dkm.cz/archlinux/$repo/os/$arch
    Server = https://archlinux.thaller.ws/$repo/os/$arch
    Server = https://phinau.de/arch/$repo/os/$arch
    Server = https://archlinux.beccacervello.it/archlinux/$repo/os/$arch
    Server = https://ftp.myrveln.se/pub/linux/archlinux/$repo/os/$arch
    Server = https://mirror.neuf.no/archlinux/$repo/os/$arch
    Server = https://mirror.efect.ro/archlinux/$repo/os/$arch
    Server = https://mirror.juniorjpdj.pl/archlinux/$repo/os/$arch
    Server = https://mirror.srv.fail/archlinux/$repo/os/$arch
    Server = https://archlinux.dynamict.se/$repo/os/$arch
    Server = https://mirrors.uni-plovdiv.net/archlinux/$repo/os/$arch
    Server = https://mirror.archlinux.no/$repo/os/$arch
    Server = https://mirror.pseudoform.org/$repo/os/$arch
    Server = https://mirrors.atviras.lt/archlinux/$repo/os/$arch
    Server = https://archlinux.mivzakim.net/$repo/os/$arch
    Server = https://pkg.fef.moe/archlinux/$repo/os/$arch
    Server = https://mirror.wdc1.us.leaseweb.net/archlinux/$repo/os/$arch
    Server = https://arch.mirror.constant.com/$repo/os/$arch
    Server = https://mirror.csclub.uwaterloo.ca/archlinux/$repo/os/$arch
    Server = https://mirrors.rit.edu/archlinux/$repo/os/$arch
    Server = https://mirror.mia11.us.leaseweb.net/archlinux/$repo/os/$arch
    Server = https://mirror.osbeck.com/archlinux/$repo/os/$arch
    Server = https://mirror.sergal.org/archlinux/$repo/os/$arch
    Server = https://mirror.sfinae.tech/pub/mirrors/archlinux/$repo/os/$arch
    Server = https://repo.ialab.dsu.edu/archlinux/$repo/os/$arch
    Server = https://mirror.dal10.us.leaseweb.net/archlinux/$repo/os/$arch
    Server = https://mirror.hackingand.coffee/arch/$repo/os/$arch
    Server = https://america.mirror.pkgbuild.com/$repo/os/$arch
    Server = https://archmirror1.octyl.net/$repo/os/$arch
    Server = https://europe.mirror.pkgbuild.com/$repo/os/$arch
    Server = https://arlm.tyzoid.com/$repo/os/$arch
    Server = https://mirror.one.com/archlinux/$repo/os/$arch
    Server = https://mirror.lty.me/archlinux/$repo/os/$arch
    Server = https://mirror.sfo12.us.leaseweb.net/archlinux/$repo/os/$arch
    Server = https://mirrors.ocf.berkeley.edu/archlinux/$repo/os/$arch
    Server = https://arch.hu.fo/archlinux/$repo/os/$arch
    Server = https://archlinux.za.mirror.allworldit.com/archlinux/$repo/os/$arch
    Server = https://archlinux.mirror.liquidtelecom.com/$repo/os/$arch
    Server = https://mirror.xtom.com.hk/archlinux/$repo/os/$arch
    Server = https://plug-mirror.rcac.purdue.edu/archlinux/$repo/os/$arch
    Server = https://asia.mirror.pkgbuild.com/$repo/os/$arch
    Server = https://mirror.fsmg.org.nz/archlinux/$repo/os/$arch
    Server = https://mirrors.ustc.edu.cn/archlinux/$repo/os/$arch
    Server = https://mirrors.hit.edu.cn/archlinux/$repo/os/$arch
    Server = https://archlinux.mirror.digitalpacific.com.au/$repo/os/$arch
    Server = https://mirrors.lug.mtu.edu/archlinux/$repo/os/$arch
    Server = https://mirrors.neusoft.edu.cn/archlinux/$repo/os/$arch
    Server = https://mirror.kumi.systems/archlinux/$repo/os/$arch
    ' | tee -a /etc/pacman.d/mirrorlist

    cp "$SCRIPT_DIR/../config/pacman-${arch}.conf" /etc/pacman.conf
fi


echo "upgrading system"
pacman-key --init
pacman -Syy --needed --noconfirm archlinux-keyring
pacman -Syyu --noconfirm
pacman -Scc --noconfirm
