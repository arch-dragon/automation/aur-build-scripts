#!/bin/bash

arch=$1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )


if [[ -z "$AUR_WAIT_TIME" ]]; then
    export AUR_WAIT_TIME=25
fi


[[ -f "generated-pipeline-$arch.yml" ]] && rm generated-pipeline-$arch.yml

touch generated-pipeline-$arch.yml
touch updatelist-$arch.txt
append="tee -a generated-pipeline-$arch.yml"
appendjob="tee -a job.yml"
appendUpdate="tee -a updatelist-$arch.txt"
pkgScriptOverrideFolder="$SCRIPT_DIR/../pkg-script-override"
defaultScriptFile="$SCRIPT_DIR/../default-script.yml"

if [ $arch = "x86_64" ]; then
    echo "image:  docker.io/ogarcia/archlinux:devel" | $append
else
    echo "image:  docker.io/ogarcia/archlinux:devel" | $append
fi

echo "stages:" | $append
echo "  - build" | $append

generated=""
while read package; do

    if [[ ! ${package:0:1} == "#" ]]; then
        cp job-template-$arch.yml job.yml

        IFS=';' read -ra packageInfo <<< "${package}"
        count=0
        packageName=${packageInfo[0]}

        echo ""
        bash -c "$SCRIPT_DIR/checkIfNeedToBeUpdated.sh $packageName $arch"
        result=$?
        if [ $result -eq 0 ]; then
            echo "$packageName is up to date"
            continue
        elif [ $result -eq 2 ]; then
            echo "$packageName is not in the AUR"
            echo "$packageName" >> notfound-$arch.txt
            continue
        else
            echo "$packageName need to be built"
        fi

        echo "$packageName " | $appendUpdate
        
        # TODO: support this in aarch64 job template
        for info in "${packageInfo[@]}"; do
            if [[ ! $count -eq 0 ]]; then        
                IFS=':' read -ra data <<< "${info}"       
                key=${data[0]}
                value=${data[1]}
                echo "" | $appendjob
                echo "    ${key}:" | $appendjob
                echo "        - ${value}" | $appendjob
            fi
            count=$count+1
        done
        #----------------------------

        scriptContent=""
        if [ -f "$pkgScriptOverrideFolder/$packageName.yml" ]; then
            scriptContent=$(cat "$pkgScriptOverrideFolder/$packageName.yml")
        else
            scriptContent=$(cat "$defaultScriptFile")
        fi

        #sed -i "s^{script}^$scriptContent^" job.yml

        scriptReplace=$(awk -v r="$scriptContent" '{gsub(/{script}/,r)}1' job.yml)
        echo "$scriptReplace"
        echo "$scriptReplace" > job.yml

        sed -i "s/{packagename}/$packageName/" job.yml
        sed -i "s/{arch}/$arch/" job.yml
        sed -i "s/{and}/\&\&/" job.yml
        
        echo "" | $append
        echo "" | $append
        cat job.yml | $append
        rm job.yml
        
        generated="true"

        sleep $AUR_WAIT_TIME
    fi

done < "packagelist_$arch"

if [ -z $generated ]; then
    echo "build-empty:" | $append
    echo "  stage: build" | $append
    echo "  timeout: 12 hours" | $append
    echo "  script:" | $append
    echo "      - echo 'All is Ok!'" | $append
fi

echo "" | $append
echo "" | $appendUpdate

